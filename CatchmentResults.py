import pandas as pd
from pathlib import Path

import CatchmentCalc
from stations import stations

import matplotlib.pyplot as plt
import matplotlib.patches as mpl_patches
import geopandas as gpd
import numpy as np
import seaborn as sns
import scipy.stats as st

ADDITIONAL_DATA_FOLDER = 'Add folder'
SAVE_PATH = 'Add folder'
CATCHMENT_SHAPEFILE = 'Add file'

class FullResults:
    RESULTS_STORAGE = ADDITIONAL_DATA_FOLDER / 'self_generated' / 'results_df.pkl'
    def __init__(self, p_fit, exclusion_length=730, recalculate_results=False):


        if recalculate_results:
            self.catchments = [CatchmentCalc.ThesisCatchmentCalc(station) for station in stations]
            self._fill_dataframe()
        else:
            try:
                self.df = pd.read_pickle(self.RESULTS_STORAGE)
            except FileNotFoundError:
                print('File not found, running fill_dataframe')
                self.catchments = [CatchmentCalc.ThesisCatchmentCalc(station) for station in stations]
                self._fill_dataframe()

        self.excluded_stations = self.df[self.df.after_drought_length < exclusion_length].index.values
        self.df = self.df[~ self.df.index.isin(self.excluded_stations)]
        self._df_to_gdf()

        self.p_fit = p_fit
        self.df['climate_cat'] = self._get_climate_cat()

        self.df = pd.concat([self.df, self.p_fit], join='inner', axis=1)

        self.df['area'] = pd.to_numeric(self.df['area'])
        self.add_omega_level()

        gdf = gpd.read_file(r'D:\Documents\0. Watermanagement\Thesis\GIS\Computed\eucalypt.geojson')
        gdf.set_index('awrc', inplace=True)
        gdf.drop('geometry', axis=1, inplace=True)

        self.df = pd.concat([self.df, gdf], join='inner', axis=1)

    def _get_climate_cat(self):
        climate_classification = pd.read_csv(
            r'D:/Documents/0. Watermanagement/Thesis/Data/koppen/climate_class.csv',
            index_col=0,
            header=None
        )

        x = np.array([4, 6, 8, 11, 14])

        climate_zones = {
            0: 'Tropical',
            1: 'Desert',
            2: 'Steppe',
            3: 'Temperate, dry summer',
            4: 'Temperate, dry winter',
            5: 'Temperate, no dry season'
        }

        climate_cat = np.digitize(climate_classification, x)
        vals = climate_cat[:, 0]
        climate_cat = [climate_zones[val] for val in vals]

        return pd.Series(climate_cat, index=climate_classification.index)

    def _fill_dataframe(self):
        '''
        Fill a DataFrame with stats per catchment.
        Catchments with a length after the drought of less than 2 years are excluded.
        '''

        burnt_file = ADDITIONAL_DATA_FOLDER / 'burned_area' / 'burned_perc.csv'
        burned_df = pd.read_csv(burnt_file, index_col=0, parse_dates=True)
        ndvi_df = pd.read_csv(
            ADDITIONAL_DATA_FOLDER / 'self_generated' / 'ndvi.csv',
            index_col=0,
            parse_dates=True
        )

        catchment_stats_list = []
        station_ids = []

        for Catchment in self.catchments:
            start, end, duration = self._fill_start_end_drought(Catchment)

            # p_fit = self._fill_pfit(Catchment)
            si = Catchment.get_seasonality_index()
            sr, sr_diff, sr_before, sr_after = self._fill_CatchmentCalc(Catchment)
            drought_length = duration.days
            after_drought_length = len(Catchment.df[end:])

            drought_intensity = Catchment.get_drought_intensity()
            drought_severity = Catchment.get_drought_severity()
            max_burned = self._get_burned_area(Catchment, burned_df)
            ndvi_diff = self._get_ndvi(Catchment, ndvi_df)
            spei_after_with_inf = Catchment.spei_12[Catchment.end:Catchment.end + pd.DateOffset(years=3)]
            spei_after = spei_after_with_inf.replace(np.inf, np.nan).mean()
            aridity_before = Catchment[:start].E_P.sum() / Catchment[:start].P.sum()
            evaporative_before = 1 - (Catchment[:start].Q.sum() / Catchment[:start].P.sum())
            aridity_diff, evaporative_diff = self._fill_diff_budyko_indices(Catchment)
            dp_star = Catchment.get_dp_star()
            ep_r = self._fill_ep_r(Catchment)
            p_mean = Catchment['P'].mean()

            catchment_stats_list.append({
                'start': start,
                'end': end,
                'si': si,
                'drought_length': drought_length,
                'after_drought_length': after_drought_length,
                'drought_intensity': drought_intensity,
                'drought_severity': drought_severity,
                'max_burned': max_burned,
                'ndvi_diff': ndvi_diff,
                'spei_after': spei_after,
                'sr': sr,
                'sr_diff': sr_diff,
                'sr_before': sr_before,
                'sr_after': sr_after,
                'aridity_before': aridity_before,
                'evaporative_before': evaporative_before,
                'aridity_diff': aridity_diff,
                'evaporative_diff': evaporative_diff,
                'dp_star': dp_star,
                'ep_r': ep_r,
                'p_mean': p_mean
            })
            station_ids.append(Catchment.Station.station_id)
            # self._fill_stats(Catchment)

        self.df = pd.DataFrame(data=catchment_stats_list)
        self.df.index = station_ids

        self.df['millenium_drought'] = self.df['start'].dt.year > 1995
        # self.df['sr_diff'] = (self.df['sr_after'] - self.df['sr']) / self.df['sr']

        self.df.to_pickle(self.RESULTS_STORAGE)

    def save_compare_low_high(self, prop, name, label, main=True, title=None, save_path=SAVE_PATH):
        ax = self.compare_low_high(prop)
        ks = self.ks_test(prop)

        p_value = round(ks[1], 3)
        if p_value < 0.001:
            p_value = '< 0.001'
        
        print(p_value)

        if title is None:
            ax.set_title(f'{name} for different groups of $F(\omega_{{after}})$')
        else:
            ax.set_title(f'{title} for different groups of $F(\omega_{{after}})$')

        fig = ax.get_figure()
        fig.suptitle('')
        ax.set_ylabel(label)

        handles = [mpl_patches.Rectangle((0, 0), 1, 1, fc="white", ec="white", 
                                         lw=0, alpha=0)]
        labels = [f'p-value KS-test = {p_value}']
        ax.legend(handles, labels, loc='best', handlelength=0, handletextpad=0)

        if main:
            fig.savefig(save_path / f"boxplot_{name.lower().replace(' ', '_')}", bbox_inches='tight')
        else:
            fig.savefig(save_path / 'appendices' / f"boxplot_{name.lower().replace(' ', '_')}", bbox_inches='tight')
        plt.close(fig)

    def add_omega_level(self):
        self.df['omega'] = np.select(
            [
                self.df['5'] < 0.1,
                self.df['5'] > 0.9
            ],
            [
                'low',
                'high'
            ],
            default='medium'
        )

    def ks_test(self, column):
        low = self.df[self.df['omega'] == 'low']
        high = self.df[self.df['omega'] == 'high']
        return st.ks_2samp(high[column], low[column])

    def compare_low_high(self, prop):
        fig, ax = plt.subplots()
        print(prop)
        ax = sns.violinplot(
            x="omega",
            y=prop,
            data=self.df,
            ax=ax,
            order=['low', 'medium', 'high'],
            bw=.2,
            cut=0,
            color=sns.color_palette()[0]
        )

        return ax

    def make_boxplot(self, stations, prop, ax):
        interest = self.df.reindex(stations)
        interest[prop].plot(kind='box', ax=ax)
        return ax

    def plot_map(self, column, **kwargs):
        ax = self._map_base()
        legend = kwargs.pop('legend', True)
        df = gpd.GeoDataFrame(self.df, geometry=self.df.geometry)
        df.plot(column=column,
                ax=ax,
                legend=legend,
                **kwargs)

        ax.axis('off')
        return ax

    def _map_base(self):
        world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

        australia = world[world['name'] == 'Australia']

        fig, ax = plt.subplots()
        australia.boundary.plot(ax=ax, edgecolor='black')
        return ax

    def _fill_start_end_drought(self, Catchment):
        Catchment.start, Catchment.end = Catchment.determine_start_end_drought()
        return Catchment.start, Catchment.end, Catchment.end - Catchment.start

    def _fill_CatchmentCalc(self, Catchment):
        GumbelCalc = CatchmentCalc.GumbelAnalysis(Catchment, Catchment.start, Catchment.end)
        sr = GumbelCalc.determine_root_zone_storage_capacity()
        sr_before, sr_after = GumbelCalc.get_median_sr()
        sr_diff = GumbelCalc.get_sr_diff()
        return sr, sr_diff, sr_before, sr_after

    def _fill_pfit(self, Catchment):
        MLE = CatchmentCalc.MaximumLikelihoodEstimation(Catchment, Catchment.start, Catchment.end)
        p_fit = MLE.get_fit()
        return p_fit

    def _fill_stats(self, Catchment):
        STS = Catchment.split_timeseries(Catchment.start, Catchment.end)
        stats = STS.basic_stats()
        return stats

    def _fill_diff_budyko_indices(self, Catchment):
        STS = Catchment.split_timeseries(Catchment.start, Catchment.end)
        return STS.diff_indices()

    def _fill_ep_r(self, Catchment):
        STS = Catchment.split_timeseries(Catchment.start, Catchment.end)
        _, e_a_p_new = STS.determine_climate_budyko()
        return STS.after.budyko()[1] - e_a_p_new

    def _get_burned_area(self, Catchment, burned_df):
        catchment_burned = burned_df[Catchment.Station.station_id]
        max_burned_perc = catchment_burned[Catchment.start:Catchment.end].max()
        return max_burned_perc

    def _get_ndvi(self, Catchment, ndvi_df):
        catchment_ndvi = ndvi_df[Catchment.Station.station_id]
        three_years = pd.DateOffset(years=3)
        before_start = Catchment.start - three_years

        after_end = Catchment.end + three_years
        ndvi_before = catchment_ndvi[before_start:Catchment.start].mean()
        ndvi_after = catchment_ndvi[Catchment.end:after_end].mean()

        return (ndvi_after - ndvi_before) / ndvi_before

    def _df_to_gdf(self):
        gdf = gpd.read_file(CATCHMENT_SHAPEFILE)
        gdf.set_index('awrc', inplace=True)

        self.df = pd.merge(gdf, self.df, right_index=True, left_index=True)

        self.df = pd.DataFrame(self.df)
