from __future__ import annotations
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy
import drought_indices
import dry_spell
import find_intercept
import budyko_formulas
import HRS
import pickle

from collections import namedtuple
from scipy import optimize

from itertools import repeat

ANDERSON_RESULT = namedtuple('Anderson_Result', ('statistic',
                                                 'critical_values',
                                                 'significance_level'))

START_END_SAVE_LOC = 'Add file'
P_FIT_SAVE_LOC = 'Add file'


class CatchmentCalc:
    def __init__(self, df):
        self.df = df.copy()

    def determine_actual_evaporation(self, inplace=False, overwrite=True, **kwargs) -> CatchmentCalc:
        '''
        Determine the actual evaporation for a catchment based on method of Nijzink (2016).
        '''

        if overwrite:
            Catchment = self._determine_e_i(inplace=inplace, **kwargs)
            E_a = Catchment['P_e'] - Catchment['Q']
        else:
            Catchment = self
            E_a = Catchment['P_e'] - Catchment['Q']

        E_a_avg = E_a.mean()

        # Subtract interception evaporation from potential evaporation (energy is already used).
        E_P_res = Catchment['E_P'] - Catchment['E_i']

        E_A = E_a_avg / E_P_res.mean() * E_P_res

        if inplace:
            Catchment['E_A'] = E_A
            return Catchment
        else:
            Catchment = CatchmentCalc(Catchment.df)
            Catchment['E_A'] = E_A
            return Catchment

    def spei(self, accumulation_period):
        return drought_indices.spei(self['P'], self['E_P'], accumulation_period)

    def determine_start_end_drought(self, start_hydro_year):

        self.spei_36 = self.spei(36)
        self.spei_12 = self.spei(12)

        # Skip first 5 years in drought determination, as these do not have sufficient data available before drought

        spei_36_skip_start = self.spei_36[self.spei_36.index.year > self.spei_36.index[0].year + 5]
        spei_12_skip_start = self.spei_12[self.spei_12.index.year > self.spei_12.index[0].year + 5]

        dry_spell_duration = dry_spell.determine_dry_spell(spei_36_skip_start, -0.5)

        start_drought = dry_spell_duration.idxmax().to_period('M')
        end_drought = start_drought + int(dry_spell_duration.max()) - 1

        start = find_intercept.search_intercept(spei_12_skip_start, start_drought)
        end = find_intercept.search_intercept(spei_12_skip_start, end_drought)

        self.start = find_intercept.snap_to_year(start_hydro_year, start)
        self.end = find_intercept.snap_to_year(start_hydro_year, end)

        return self.start, self.end

    def get_drought_severity(self, start: pd.datetime = None, end: pd.datetime = None,
                             spei: pd.Series = None) -> float:
        """
        Function to combine duration and intensity of the drought.
        The way it works is by summing all the SPEI values during drought.

        Parameters
        ----------
        start : pd.datetime, optional
            start of drought, by default None
        end : pd.datetime, optional
            end of drought, by default None
        spei : pd.Series, optional
            a series of SPEI values, by default None

        Returns
        -------
        float
            a value for the combined drought duration intensity
        """
        try:
            if start is None:
                start = self.start

            if end is None:
                end = self.end
        except AttributeError:
            start, end = self.determine_start_end_drought()

        if spei is None:
            spei = self._get_spei()

        duration_intensity = - spei[start:end].sum()

        return duration_intensity

    def get_drought_intensity(self, start: pd.datetime = None, end: pd.datetime = None,
                              spei: pd.Series = None) -> float:

        """
        Function to get the drought severity.
        The drought severity is defined as the lowest SPEI value during the drought.

        Parameters
        ----------
        start : pd.datetime, optional
            start of drought, by default None
        end : pd.datetime, optional
            end of drought, by default None
        spei : pd.Series, optional
            a series of SPEI values, by default None

        Returns
        -------
        float
            a value for the combined drought duration intensity
        """

        if start is None:
            start = self.start

        if end is None:
            end = self.end

        if spei is None:
            spei = self._get_spei()

        drought_severity = - spei[start:end].min()

        return drought_severity

    def get_dp_star(self):
        dp, sp = self._least_squares_optimization('P')
        de, se = self._least_squares_optimization('E_P')
        print(dp)
        return dp * np.sign(de) * np.cos(2 * np.pi * (sp - se)/365)

    def _least_squares_optimization(self, variable):
        pars = optimize.least_squares(self._get_difference_estimate_reality, (1, 0), args=[variable]).x
        return pars

    def _get_difference_estimate_reality(self, pars, variable):
        P_est = self._get_daily_estimates(pars, variable)
        return self.df[variable] - P_est

    def _get_daily_estimates(self, pars, var):
        dp, sp = pars
        s = self.df[var]
        P_mean = s.mean()
        t = s.index.dayofyear.values
        if dp > 0:
            cr = -0.001 * dp**4 + 0.026 * dp ** 3 - 0.245 * dp ** 2 + 0.2432 * dp - 0.038
        else:
            cr = 0
        addition = (1 + cr + dp * np.sin(2*np.pi*(t - sp) / 365))
        P_est = P_mean * addition
        P_est[P_est < 0] = 0
        return P_est

    def _get_spei(self):
        try:
            spei = self.spei_12
        except AttributeError:
            self.spei_12 = self.spei(12)
            spei = self.spei_12
        return spei

    def budyko(self) -> tuple:
        '''
        Determine the aridity index and evaporative index
        '''
        Q = self['Q'].sum()
        P = self['P'].sum()
        E_P = self['E_P'].sum()
        aridity = E_P / P
        evaporative = 1 - (Q / P)
        return np.array((aridity, evaporative))

    def deficit(self, overwrite=True, **kwargs) -> pd.Series:
        '''
        Determine the daily storage deficit based on the Mass Curve Technique (Gao, 2014).
        '''
        if overwrite:
            df = self.determine_actual_evaporation(**kwargs).df
        else:
            df = self.df

        storage = (df['P'] - df['E_A'])
        total = 0
        cumulative_deficit = []

        for row in storage:
            new_total = row + total
            total = min(new_total, 0)
            cumulative_deficit.append(total)

        return pd.Series(cumulative_deficit, index=df.index, name='deficit')

    def waterbalance(self) -> float:
        '''
        Determine the long-term waterbalance of the catchment (P - Q - E_P).
        Value should be negative (E_P > E_A)
        '''
        P_tot = self.df.P.sum()
        E_P_tot = self.df.E_P.sum()
        Q_tot = self.df.Q.sum()
        dS = (P_tot - E_P_tot - Q_tot)
        return dS

    def split_timeseries(self, start_date: pd.Datetime, end_date: pd.datetime) -> SplittedTimeseries:
        '''
        Split a timeseries into before, during and after drought based on a start and end date.
        '''
        return SplittedTimeseries(self, start_date, end_date)

    def get_seasonality_index(self, parameter='P'):
        '''
        Determine the seasonality index based on Gao (2014)

        Parameters
        ----
        parameter = the parameter for which to determine seasonality index. Default = P

        Returns
        ----
        seasonality index = The seasonality index of the catchment
        '''

        annual = self.df.groupby('hydrologic_year')[parameter].sum()
        annual_mean = annual.mean()

        monthly = self.df.groupby(pd.Grouper(freq='M'))[parameter].sum()
        monthly_mean = monthly.groupby(monthly.index.month).mean()

        self.seasonality_index = 1 / annual_mean * (monthly_mean - (annual_mean / 12)).abs().sum()

        return self.seasonality_index

    def determine_w(self):
        '''
        Give the best fit for the w value for a pair of evaporative index and aridity index
        '''
        aridity, evaporative_index = self.budyko()
        w = scipy.optimize.minimize_scalar(self._determine_fit_error, bounds=[1, 20], method='Bounded',
                                           args=(aridity, evaporative_index))
        return w.x

    def _determine_e_i(self, I_max=2, inplace=False) -> CatchmentCalc:
        '''
        TODO: Determine I_max
        Determine the interception evaporation for a catchment. This will be used for determining actual evaporation.
        Assumption is that all values are in mm/day and timesteps are equidistant.

        Parameters:
        I_max = maximum interception capacity in mm (default = 2 mm)
        '''

        dt = (self.df.index[1] - self.df.index[0]).days
        P_all = self.df['P'].values
        E_P_all = self.df['E_P'].values
        S = np.zeros_like(E_P_all)
        P_E_all = np.zeros_like(E_P_all)
        E_i_all = np.zeros_like(E_P_all)

        for i, (P, E_P) in enumerate(zip(P_all, E_P_all)):

            # For the first timestep the initial storage is equals to the precipitation
            # For subsequent timesteps, the storage is equal to the sum of previous storage and precipitation
            if i > 0:
                S_in = S[i-1] + P * dt
            else:
                S_in = P * dt

            # Effective precipitation equals the overflow from the interception storage
            P_e = max(0, (S_in - I_max) / dt)
            S_in -= P_e

            # Evaporation from evaporation bucket is equal to the potential evaporation. If storage is smaller
            # than E_P, it is equal to the storage
            E_i = min(S_in, E_P * dt)
            S[i] = S_in - E_i
            P_E_all[i] = P_e
            E_i_all[i] = E_i

        # If inplace, the class is updated, otherwise a new class is created with an updated dataframe
        if inplace:
            self.df['S_i'] = S
            self.df['P_e'] = P_E_all
            self.df['E_i'] = E_i_all
            return self
        else:
            df = self.df.copy()
            df['S_i'] = S
            df['P_e'] = P_E_all
            df['E_i'] = E_i_all
            return CatchmentCalc(df)

    def _determine_fit_error(self, w, epp, eap):
        '''
        Error squared, because than minimize can be used
        fsolve gave strange values. Also see:
        https://github.com/scipy/scipy/issues/3887
        '''
        return (budyko_formulas.fu(epp, w) - eap)**2

    def __getitem__(self, position):
        return self.df[position]

    def __setitem__(self, position, item):
        self.df[position] = item

    def __len__(self):
        return len(self.df)


class ThesisCatchmentCalc(CatchmentCalc):
    def __init__(self, station_id):
        self.Station = HRS.ConvertStationData(station_id)
        super().__init__(self.Station.df)

    def determine_start_end_drought(self):

        # Load a pickle with start end dates, because SPEI takes long to calculate
        file_loc = START_END_SAVE_LOC
        try:
            with open(file_loc, 'rb') as pickle_file:
                start_end_file = pickle.load(pickle_file)
        except FileNotFoundError:
            start_end_file = {}

        # If station can not be found in the start_end dictionary, it is determined from data and written to file
        try:
            self.start, self.end = start_end_file[self.Station.station_id]
        except KeyError:
            start, end = super().determine_start_end_drought(self.Station.start_hydro_year)
            start_end_file[self.Station.station_id] = (start, end)
            with open(file_loc, 'wb') as pickle_file:
                pickle.dump(start_end_file, pickle_file)

        return self.start, self.end


class SplittedTimeseries:
    def __init__(self, original, start_date, end_date, num_years=10):
        self.original = original
        self.num_years = num_years
        hydrologic_year_end_drought = self.original.df.loc[end_date].hydrologic_year
        self.before = CatchmentCalc(original[original.df.index.to_period('M') < start_date])
        self.after = CatchmentCalc(original[
            (original.df.index.to_period('M') >= end_date) &
            (original.df['hydrologic_year'] < hydrologic_year_end_drought+self.num_years)
            ]
        )
        self.during = CatchmentCalc(original[(original.df.index.to_period('M') >= start_date) &
                                             (original.df.index.to_period('M') < end_date)])
        self.all_periods = [self.before, self.during, self.after]

        self.start = start_date
        self.end = end_date
        self.colors = 'grb'
        self.labels = ['Before', 'During', 'After']

    def determine_climate_budyko(self):
        w = self.before.determine_w()
        e_p_p, self.e_a_p_after = self.after.budyko()
        self.e_a_p_new = budyko_formulas.fu(e_p_p, w)
        return np.array([e_p_p, self.e_a_p_new])

    def determine_budyko_diff(self, climate=False):
        if climate:
            budyko = self.determine_climate_budyko()
        else:
            budyko = self.after.budyko()
        diff = budyko - self.before.budyko()
        dist = np.linalg.norm(diff)
        angle = np.arctan2(diff[1], diff[0])
        return (dist, angle)

    def plot_radials(self, ax=None, climate=False, **kwargs):
        if ax is None:
            fig, ax = plt.subplots(subplot_kw=dict(polar=True))
        dist, angle = self.determine_budyko_diff(climate)

        ax.scatter(angle, dist, **kwargs)

        return ax

    def plot_budyko(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
        for i, period in enumerate(self.all_periods):
            ax.scatter(*period.budyko(), color=self.colors[i], label=self.labels[i])
        ax.legend()

        ax.set_ylim(0, 1.1)

        x_ax = ax.get_xlim()
        ax.set_xlim(0, x_ax[1])
        ax.plot([0, 1], [0, 1], color='black')
        ax.axhline(y=1, xmin=(1)/(x_ax[1]), color='black')
        ax.set_xlabel('Aridity index')
        ax.set_ylabel('Evaporative index')
        try:
            return fig, ax
        except UnboundLocalError:
            return ax

    def plot_timeseries(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()

        for i, period in enumerate(self.all_periods):
            period['Q'].plot(ax=ax, color=self.colors[i], label=self.labels[i])
        for i, period in enumerate(self.all_periods):
            ax.axhline(period['Q'].mean(), color=self.colors[i], alpha=0.8, linestyle='--')

        ax.legend()
        ax.set_ylabel('Daily discharge [mm]')
        ax.set_title('Timeseries splitted by drought')
        return ax

    def basic_stats(self):
        '''
        Basic statistics before drought for use in determining correlations
        '''

        description = self.before.df.describe()[['E_P', 'P', 'Q']]

        return description.stack().T

    def diff_indices(self):
        budyko_before = self.before.budyko()
        budyko_after = self.after.budyko()

        return (budyko_after - budyko_before) / budyko_before


class MaximumLikelihoodEstimation(SplittedTimeseries):

    def __init__(self, original, start_date, end_date, num_years=7, dist='gamma'):
        dist_dict = {
            'gamma': scipy.stats.gamma,
            'lognorm': scipy.stats.lognorm
        }
        super().__init__(original, start_date, end_date, num_years)
        self.dist = dist_dict[dist]

    def likelihood_estimation(self, **kwargs):
        '''
        Utility function for performing the estimation in one step,
        instead of using multiple calls.
        '''

        num_samples = kwargs.pop('num_samples', 1000)
        fit_to_dist = kwargs.pop('fit_to_dist', True)

        self.sample_data(num_samples)
        self.determine_w_all()
        self.determine_fit(fit_to_dist=fit_to_dist)
        return self.p_fit

    def sample_data(self, num_samples=1000):
        '''
        Sample data following the bootstrap method and return the aridity and evaporative index belonging to this
        TODO: Make variable sample_len en num_samples
        '''
        self._bootstrap_samples(sample_len=self.num_years, num_samples=num_samples)
        self.budyko_values = np.apply_along_axis(self._determine_aridity_and_evaporation_from_sample,
                                                 1, self.samples)[:]

    def determine_w_all(self, budyko_values=None):
        '''
        Determine the w parameter for all samples
        '''
        if budyko_values is None:
            budyko_values = self.budyko_values
        self.w_all = np.apply_along_axis(self._determine_w, 1, budyko_values)
        return self.w_all

    def get_fit(self, recalculate=False):
        '''
        Get the p_fit for the catchment.
        If it is not determined yet or if recalculate is True, it will be determined using p_fit
        Otherwise, it will be retrieved from a file.
        '''

        self.p_fit_file_loc = P_FIT_SAVE_LOC
        try:
            with open(self.p_fit_file_loc, 'rb') as pickle_file:
                p_fit_file = pickle.load(pickle_file)
        except FileNotFoundError:
            p_fit_file = {}

        if recalculate:
            self.sample_data()
            self.determine_w_all()
            self.p_fit = self._retrieve_p_fit(p_fit_file)
        else:
            # If station can not be found in the p_fit dictionary, it is determined from data and written to file
            try:
                self.p_fit = p_fit_file[self.original.Station.station_id]
            except KeyError:
                self.sample_data()
                self.determine_w_all()
                self.p_fit = self._retrieve_p_fit(p_fit_file)
        return self.p_fit

    def anderson(self):
        # From: https://www.ine.pt/revstat/pdf/rs160102.pdf
        sig = np.array([20, 15, 10, 5])
        _Avals = np.array([1.410, 1.617, 1.904, 2.399])
        w_all_sorted = np.sort(self.w_all)
        n = len(w_all_sorted)
        cdf_val = self.dist.cdf(w_all_sorted, *self.pars)
        i = np.arange(1, n + 1)
        bounds = _Avals / (1 + 0.2 / (n)**0.5)

        sum_val = np.sum((2*i - 1.0) / n * (np.log(cdf_val) + np.log(1 - cdf_val[::-1])))
        A2 = -n - sum_val
        return ANDERSON_RESULT(A2, bounds, sig)

    def determine_fit(self, w_all=None, fit_to_dist=True):
        if w_all is None:
            w_all = self.w_all

        self.w_after = self.after.determine_w()

        if fit_to_dist:
            self.pars = self.dist.fit(w_all)
            self.p_fit = self.dist.cdf(self.w_after, *self.pars)

        else:
            w_all.sort()
            num_smaller = len(w_all[w_all < self.w_after])
            self.p_fit = num_smaller / len(w_all)

        return self.p_fit

    def plot_ecdf(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
        w_all_sorted = np.sort(self.w_all)
        ax.plot(w_all_sorted, np.linspace(0, 1, len(self.w_all), endpoint=False), 'r--')

        return ax

    def plot_cdf(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
        dist = self.dist(*self.pars)
        w_lin = np.linspace(0, 5, 100)
        y = dist.cdf(w_lin)
        ax.plot(w_lin, y, label=self.anderson()[0])

        return ax

    def _retrieve_p_fit(self, p_fit_file):
        p_fit = self.determine_fit()
        p_fit_file[self.original.Station.station_id] = p_fit
        with open(self.p_fit_file_loc, 'wb') as pickle_file:
            pickle.dump(p_fit_file, pickle_file)
        return p_fit

    def _determine_w(self, budyko_list):
        '''
        Give the best fit for the w value for a pair of evaporative index and aridity index
        '''
        aridity, evaporative_index = budyko_list
        w = scipy.optimize.minimize_scalar(self._determine_fit_error, bounds=[1, 20], method='Bounded',
                                           args=(aridity, evaporative_index))
        return w.x

    def _determine_fit_error(self, w, epp, eap):
        '''
        Error squared, because than minimize can be used
        fsolve gave strange values. Also see:
        https://github.com/scipy/scipy/issues/3887
        '''
        return (budyko_formulas.fu(epp, w) - eap)**2

    def _bootstrap_samples(self, sample_len=5, num_samples=1000):
        '''
        Mostly copied from boxplots.py
        '''

        # Only choose full hydrological years
        all_years, counts = np.unique(self.before.df.hydrologic_year.values, return_counts=True)
        all_years = all_years[counts >= 365]

        # Generate samples
        self.samples = np.random.choice(all_years, (num_samples, sample_len), replace=True)

    def _determine_aridity_and_evaporation_from_sample(self, sample):
        unique, counts = np.unique(sample, return_counts=True)
        # If there are only unique values, one df can be kept. Otherwise df has to be made per year and concatenated
        if (counts > 1).any():
            df_list = []
            for year, count in zip(unique, counts):
                to_add = repeat(self.before.df[self.before.df.hydrologic_year == year], count)
                df_list.extend(to_add)
            df = pd.concat(df_list)
        else:
            df = self.before.df[self.before.df.hydrologic_year.isin(unique)]

        P_sum = df['P'].sum()
        evaporative_index = 1 - df['Q'].sum() / P_sum
        aridity = df['E_P'].sum() / P_sum

        return aridity, evaporative_index


class GumbelAnalysis(SplittedTimeseries):

    def __init__(self, original, start_date, end_date, I_max=2, return_period=20):
        self.original = original
        self._set_deficit_year()
        self.distribution = scipy.stats.gumbel_r
        self.I_max = I_max
        self.return_period = return_period
        super().__init__(original, start_date, end_date)

    def determine_root_zone_storage_capacity(self):
        self.determine_max_deficit_by_year()
        self.fit_to_distribution()
        self.return_period_from_distribution(self.return_period)
        return self.root_zone_storage_capacity

    def fit_through_recovery(self):
        pars = self.distribution.fit(self.recovery())
        p_non_exceedance = 1 - 1 / self.return_period
        self.rz_after = self.distribution.ppf(p_non_exceedance, *pars)
        return self.rz_after

    def get_sr1yr_array(self, period):
        """
        Determine an array for values of sr1yr for a given period.
        This period is before or after the drought

        Parameters
        ----------
        period : CatchmentCalc
            A period before or after drought

        Returns
        ---------
        sr1yr_array : np.array
            An array with all sr1yr values
        """
        unique_years = period['deficit_year'].unique()
        sr1yr_array = np.array([self.determine_sr1yr(year) for year in unique_years[:-2]])
        return sr1yr_array

    def get_median_sr(self):
        """
        Determine the sr_diff value as described in report.

        """
        before_array = self.get_sr1yr_array(self.before)
        after_array = self.get_sr1yr_array(self.after)

        sr_before = np.nanmedian(before_array)
        sr_after = np.nanmedian(after_array)

        return sr_before, sr_after

    def get_sr_diff(self):
        sr_before, sr_after = self.get_median_sr()
        return (sr_after - sr_before)

    def recovery(self):
        self.unique_years_after = self.after['deficit_year'].unique()
        # Currently only for the first 7 years after droughtn (maximum)
        rz_oneyr = np.array([self.determine_sr1yr(year)
                            for i, year in enumerate(self.unique_years_after[1:8])])

        rz_oneyr = rz_oneyr[~np.isnan(rz_oneyr)]
        return rz_oneyr

    def determine_sr1yr(self, year):
        subset_catchment = self._make_subset_class(3, year)
        deficit = subset_catchment.deficit(inplace=True, I_max=self.I_max)

        # Select deficit for only the current year
        df = deficit[subset_catchment['deficit_year'] == year]

        # Only consider full years
        if len(df) > 364:
            root_zone_storage_capacity = self._determine_max_yearly_deficit(df)
        else:
            root_zone_storage_capacity = np.nan
        return root_zone_storage_capacity

    def plot_ea_diff(self, length):
        fig, ax = plt.subplots()
        unique_years_after = self.after['deficit_year'].unique()
        for i, year in enumerate(unique_years_after):
            Catchment = self._make_subset_class(length, year)
            Catchment.deficit(inplace=True)
            monthly_ea = Catchment['E_A'].groupby(pd.Grouper(freq='M')).sum()
            ax.plot(monthly_ea, label=year)

        return ax

    def determine_max_deficit_by_year(self, I_max=None):
        if I_max is None:
            I_max = self.I_max
        deficit = self.before.deficit(I_max=I_max)
        yearly_deficit_group = deficit.groupby(self.before['deficit_year'])
        self.yearly_max_deficits = yearly_deficit_group.apply(self._determine_max_yearly_deficit)
        return self.yearly_max_deficits

    def fit_to_distribution(self, yearly_max_values=None):
        '''
        Currently a right-skewed gumbel distribution is used. However, as seen before this may not be ideal.
        It has to be checked whether for other catchments exponweib performs better as well.
        Is there a certain metric for this?
        '''
        if yearly_max_values is None:
            yearly_max_values = self.yearly_max_deficits

        self.pars = self.distribution.fit(yearly_max_values)

        return self.pars

    def compare_distributions(self):
        fig, ax = plt.subplots()
        x, y = self._determine_exceedance()
        ax.plot(x, y, 'o', label='deficits')
        ax = self._plot_fit('gumbel', ax=ax)
        ax = self._plot_fit('exponweib', ax=ax)
        ax = self._plot_fit('genextreme', ax=ax)
        ax.legend()
        # ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_xlabel('Cumulative deficit [mm]')
        ax.set_ylabel('Return period [years]')
        ax.set_title('Fit for different distributions')
        return ax

    def return_period_from_distribution(self, return_period):
        p = 1/return_period
        self.root_zone_storage_capacity = self.distribution.ppf(1-p, *self.pars)
        return self.root_zone_storage_capacity

    def _determine_max_yearly_deficit(self, group):
        """
        Determine the difference between the maximum deficit and the minimum deficit before the maximum.

        Parameters
        ----------
        group : pd.Series
            A Pandas Series containing one hydrological year.

        Returns
        -------
        deficit_difference
            Difference between minimum and maximum deficit.

        """
        group_df = group[group.index <= group.idxmin()]
        return group_df.max() - group_df.min()

    def _set_deficit_year(self):
        self._define_start_deficit_year()
        df = self.original.df
        index_month = df.index.month
        if self.start_month > 8:
            df.loc[index_month >= self.start_month, 'deficit_year'] = df[index_month >= self.start_month].index.year + 1
            df.loc[index_month < self.start_month, 'deficit_year'] = df[index_month < self.start_month].index.year
        else:
            df.loc[index_month >= self.start_month, 'deficit_year'] = df[index_month >= self.start_month].index.year
            df.loc[index_month < self.start_month, 'deficit_year'] = df[index_month < self.start_month].index.year - 1

        df['deficit_year'] = df['deficit_year'].astype(int)

    def _define_start_deficit_year(self):
        month_max = self.original.deficit().groupby(pd.Grouper(freq='M')).min()
        self.start_month = month_max.groupby(month_max.index.month).sum().idxmax()

    def _determine_exceedance(self):
        x = self.yearly_max_deficits.sort_values(ascending=False)

        p = 1.-np.arange(1., len(x) + 1.)/len(x)
        T = 1/(1-p)
        return x.values, T

    def _plot_fit(self, distribution, ax=None, **kwargs):
        st = scipy.stats
        if ax is None:
            fig, ax = plt.subplots()
        distributions = {
            'gamma': st.gamma,
            'gumbel': st.gumbel_r,
            'exponweib': st.exponweib,
            'genextreme': st.genextreme,
            'weibull': st.weibull_min}
        dist = distributions[distribution]

        pars = dist.fit(self.yearly_max_deficits, **kwargs)
        x = np.linspace(dist.ppf(0.01, *pars),
                        dist.ppf(0.99, *pars), 100)
        ax.plot(x, 1/(1-dist.cdf(x, *pars)),
                '-', label=f'fit to {distribution}')
        return ax

    def _make_subset_class(self, length, start_year):
        """
        Create a subset of the full dataset and create a CatchmentCalc class with this data

        Parameters
        ----------
        length : int
            length to take into account
        start_year : int
            start year of the subset


        Returns
        -------
        subset_rz : CatchmentCalc.CatchmentCalc
            CatchmentCalc class with only the years in the subset
        """

        df = self.original[self.original['deficit_year'].isin(range(start_year, start_year+length))]
        subset_rz = CatchmentCalc(df)
        return subset_rz
