import requests
import io
import json
import pandas as pd
import calendar

from shapely.geometry import shape
from json import JSONDecodeError
from pathlib import Path


class LoadStationData:
    _BASE_PATH = 'Add folder'

    def __init__(self, station_id):
        self.station_id = station_id
        self.station_attributes = self.get_station_attributes()
        try:
            self.local_id = int(self.station_attributes.get('local_id')[0])
        except ValueError:
            self.local_id = self.station_attributes.get('local_id')

        self.area = float(self.station_attributes.get('area')[0])
        self.shapefile = self.get_shapefile()
        self.runoff = self.get_runoff_timeseries()
        self.precipitation = self.get_silo('daily_rain')
        self.potential_evaporation = self.get_silo('et_morton_potential')

    def get_shapefile(self):
        '''
        Try to get the json with geodata from the local folder.
        If it does not exist for the current station id it will be downloaded from the HRS server
        and saved in the local folder.
        Returns a shapely shape of the json file.
        '''
        filename = self._BASE_PATH / 'shapes' / f'{self.station_id}.json'

        try:
            with filename.open() as f:
                data = json.load(f)
        except FileNotFoundError:
            try:
                url = f'http://www.bom.gov.au/water/hrs/content/gis/{self.station_id}.json'
                response = requests.get(url)
                data = response.json()
            except JSONDecodeError:
                url = f'http://www.bom.gov.au/water/hrs/content/gis/{self.local_id}.json'
                response = requests.get(url)
                data = response.json()

            filename.parent.mkdir(parents=True, exist_ok=True)

            with filename.open('w') as f:
                json.dump(data, f)
        return shape(data)

    def get_station_attributes(self):
        '''
        Try to get the json with station data from the local folder.
        If it does not exist for the current station id it will be downloaded from the HRS server
        and saved in the local folder.
        Returns a dictionary with the station attributes.
        '''

        filename = self._BASE_PATH / 'attributes' / f'{self.station_id}.json'

        try:
            with filename.open() as f:
                attributes = json.load(f)
        except FileNotFoundError:
            url = f'http://www.bom.gov.au/water/hrs/content/json/{self.station_id}/{self.station_id}_station_attributes.json'  # noqa
            response = requests.get(url)
            attributes = response.json()

            filename.parent.mkdir(parents=True, exist_ok=True)
            with filename.open('w') as f:
                json.dump(attributes, f)

        return attributes

    def get_runoff_timeseries(self):
        '''
        Try to get the csv file with runoff data from the local folder.
        If it does not exist for the current station id it will be downloaded from the HRS server
        and saved in the local folder.
        Returns a pd dataframe with the runoff in GL.
        '''
        filename = self._BASE_PATH / 'runoff' / f'{self.station_id}.csv'
        try:
            runoff = pd.read_csv(filename, index_col=0, parse_dates=True)
        except FileNotFoundError:
            url = f'http://www.bom.gov.au/water/hrs/content/data/{self.station_id}/{self.station_id}_daily_ts.csv'
            response = requests.get(url)
            runoff = pd.read_csv(io.StringIO(response.text), skiprows=26,
                                 index_col=0, parse_dates=True)

            filename.parent.mkdir(parents=True, exist_ok=True)
            runoff.to_csv(filename)

        return runoff

    def get_silo(self, parameter):
        '''
        Get the SILO aggregated data. These files are created in a different script.
        Input is a name of a silo parameter.
        Returns a pandas series with daily values for the parameters.
        '''
        filename = self._BASE_PATH.parent / 'SILO_agg' / parameter / (self.station_id + '.csv')
        df = pd.read_csv(filename, index_col=0, parse_dates=True)
        return df.iloc[:, 0]


class ConvertStationData(LoadStationData):
    def __init__(self, station_id):
        super().__init__(station_id)
        self.converted_runoff = self._ml_to_mm()
        self.df = self.create_full_dataframe()

    def create_full_dataframe(self):
        '''
        Create a dataframe with precipitation, discharge and
        Morton potential evaporation from the already known data
        and add the hydrologic year
        '''
        df = pd.concat([self.precipitation, self.converted_runoff, self.potential_evaporation], axis=1, join='inner')
        df.columns = ['P', 'Q', 'E_P']
        df = self._add_hydrologic_year(df)
        return df

    def make_eda_plots(self):
        '''
        Make EDA plots to check the given data.
        Plots consists of a quality code bar chart,
        a monthly rainfall-runoff series,
        autocorrelation plots for discharge and precipitation
        and a scatter matrix plot.
        '''

        import matplotlib.pyplot as plt

        # QCode plot
        fig, ax = plt.subplots()
        qcode = self.runoff['Bureau QCode'].astype('category')
        qcode.cat.set_categories(['A', 'B', 'C', 'E', 'G'], inplace=True)
        qcode.value_counts().sort_index().plot(kind='bar', stacked=True, ax=ax)

        # Rainfall runoff plot
        fig, ax1 = plt.subplots()
        monthly_df = self.df.resample('M').sum()
        ax1.plot(monthly_df['Q'], 'orange', label='Observed discharge')
        ax1_y = ax1.get_ylim()
        ax1.set_ylim(ax1_y[0], ax1_y[1] * 1.5)

        ax2 = ax1.twinx()
        ax2.plot(monthly_df['P'], label='Precipitation', alpha=0.6, zorder=0)
        ax2_y = ax2.get_ylim()
        ax2.set_ylim([ax2_y[1] + 0.5*ax2_y[1], ax2_y[0]])
        ax1.set_zorder(ax2.get_zorder()+1)
        ax1.patch.set_visible(False)

        ax1.set_title('Rainfall runoff')
        lines1, labels1 = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax1.legend(lines1 + lines2, labels1 + labels2, loc=0).set_zorder(2)

        # Discharge autocorrelation plot
        fig, ax = plt.subplots()
        pd.plotting.autocorrelation_plot(self['Q'], ax=ax)
        ax.set_title('Autocorrelation plot for discharge')

        # Precipitation autocorrelation plot
        fig, ax = plt.subplots()
        pd.plotting.autocorrelation_plot(self['P'], ax=ax)
        ax.set_title('Autocorrelation plot for precipitation')

        # Scatter matrix plot
        to_plot = self.df.resample('M').sum()
        to_plot['hydrologic_year'] = self.df.resample('M').mean()['hydrologic_year']
        pd.plotting.scatter_matrix(to_plot, diagonal='kde')

    def _ml_to_mm(self):
        '''
        From ML to mm. Area is given in km2 = 10 ** 6 m2
        ML = 1000 m3
        '''
        to_meter = self.runoff['Flow (ML)'] * 1000 / (self.area * 10**6)
        to_mm = to_meter * 1000
        return to_mm

    def _add_hydrologic_year(self, df):

        # Make a library with short month names and month number.
        # This is used to get a number for the start of the hydrologic year.
        name_to_num = {v: k for k, v in enumerate(calendar.month_abbr)}
        start_month = self.station_attributes['water_year_short'][0].split()[0]
        start_month = name_to_num[start_month]

        # If hydrologic year starts after june, hydrologic year is the year after the starting month.
        # Else it is the year of the starting month
        if start_month > 6:
            df.loc[df.index.month >= start_month, 'hydrologic_year'] = df[df.index.month >= start_month].index.year + 1
            df.loc[df.index.month < start_month, 'hydrologic_year'] = df[df.index.month < start_month].index.year
        else:
            df.loc[df.index.month >= start_month, 'hydrologic_year'] = df[df.index.month >= start_month].index.year
            df.loc[df.index.month < start_month, 'hydrologic_year'] = df[df.index.month < start_month].index.year - 1

        df['hydrologic_year'] = df['hydrologic_year'].astype(int)
        self.start_hydro_year = start_month
        return df

    def __getitem__(self, position):
        return self.df[position]

    def __len__(self):
        return len(self.df)
