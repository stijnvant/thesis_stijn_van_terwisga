# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 10:13:35 2019

@author: Stijn
"""
import pandas as pd


def determine_dry_spell(series: pd.Series, threshold: float) -> pd.Series:
    df = series.to_frame()
    df.columns = ['drought_index']

    df['negative'] = (df['drought_index'] < threshold).astype(int)
    df['groups'] = df['negative'].diff().abs().cumsum()
    dry_spell = df.negative * df.groupby(df.groups)['drought_index'].transform('count')
    return dry_spell
