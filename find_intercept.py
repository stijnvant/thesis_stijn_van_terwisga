# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 13:39:29 2019

@author: Stijn
"""

import pandas as pd
# %%


def search_intercept(spei_12, intercept_36):
    monthly_index = spei_12.index.to_period('M')

    begin_search = pd.to_datetime(f'{intercept_36.year - 3}-{intercept_36.month}').to_period('M')
    poi = spei_12.loc[(monthly_index > begin_search) & (monthly_index < intercept_36)]
    index_start = (poi[::-1] > -.5).shift(-1).dropna().astype(int).idxmax()
    intercept_12 = index_start - pd.DateOffset(years=1)
    return intercept_12

# %%


def snap_to_year(start_hydro_year, initial_date):
    x = pd.to_datetime(f'01-{start_hydro_year}-{initial_date.year}', dayfirst=True)
    diff = initial_date - x
    diff = diff.days

    max_diff = 183

    if diff < max_diff:
        x
    elif diff < 0:
        x -= pd.DateOffset(years=1)
    else:
        x += pd.DateOffset(years=1)

    return x
