import numpy as np

def zhang(e_p_p: float, theta: float) -> np.array:
    e_a_p = (1+theta*e_p_p)/(1+theta*e_p_p + 1/e_p_p)
    
    return e_a_p

def schreiber(e_p_p: float) -> np.array:
    e_a_p = 1 - np.exp(-e_p_p)
    return e_a_p

def pike(e_p_p: float) -> np.array:
    e_a_p = 1/(np.sqrt(1+np.square(1/e_p_p)))
    return e_a_p

def fu(epp: float, w: float) -> float:
    return 1 + epp - (1 + epp**w)**(1/w)