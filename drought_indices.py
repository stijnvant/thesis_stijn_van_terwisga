# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 09:04:16 2019

@author: Stijn
"""

import numpy as np
import pandas as pd
import scipy.stats as st

from calendar import monthrange


def hdi(p_a, e_a_a):
    '''
    Currently determines annually, need to look into this further to determine
    it exactly.
    '''
    hdi = p_a / e_a_a
    hdi_diff = hdi / hdi.mean()
    return hdi_diff


def dri(p_a, e_p_a):
    '''
    Determine the reconnaiscance drought index from the yearly potential
    evaporation and yearly precipitation.
    '''
    alpha = p_a / e_p_a
    dri = alpha / alpha.mean() - 1
    return dri


def deciles(p_a):
    percentage = ((p_a.rank() / len(p_a)*10))
    decile = np.ceil(percentage).astype(int)
    return decile


def check_full_month(series, index):
    year, month = (series.index[index].year, series.index[index].month)
    length = (series[(series.index.year == year) & (series.index.month == month)]).count()
    if length != monthrange(year, month)[1]:
        series = series[~((series.index.year == year) & (series.index.month == month))]
    return series


def spei(p_d: pd.Series, e_p_d: pd.Series, acc_time: int) -> pd.Series:
    '''
    Daily values of precipitation and potential evaporation to monthly spei
    '''
    D = p_d - e_p_d
    D = check_full_month(D, 0)
    D = check_full_month(D, -1)

    D_m = D.resample('M').sum()
    D_rolling = D_m.rolling(acc_time).sum()

    grouped_by_month = D_rolling.groupby(D_rolling.index.month)
    prob = grouped_by_month.rank(pct=True)

    spei = pd.Series(index=prob.index)

    for month in range(1, 13):
        deltapep = D_rolling[prob.index.month == month]
        pars = st.genextreme.fit(deltapep, loc=deltapep.median(), scale=deltapep.std())
        fit = st.genextreme.cdf(deltapep, pars[0], pars[1], pars[2])
        spei.loc[prob.index.month == month] = st.norm.ppf(fit, 0, 1)

    return spei
